$(document).ready(function(){
    $.ajaxSetup({
        headers: {  'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content') }
    });
    $('[data-js="upload"]').click(function () {
        $('#fileUpload').modal('show');

        $('#files').fileinput({
            uploadUrl: '/admin/uploadFile/' + $(this).data('scheludeid'),
            ajaxSettings: {
                headers: {

                }
            }
        })
    })
})