$(document).ready(function () {
    var $checkbox = $('[data-js="check-study"]')
    
    $checkbox.click(function (e) {
        var id = $(this).val();
        if ($(this).prop('checked')){
            $('[name="time['+ id +']"]').removeAttr('disabled');
            $('[name="ages['+ id +']"]').removeAttr('disabled');
        } else {
            $('[name="time['+ id +']"]').attr('disabled','disabled');
            $('[name="ages['+ id +']"]').attr('disabled','disabled');
        }
    })
})