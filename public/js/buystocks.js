$(document).ready(function(){
    var price = $('#price').val();
    var balance = $('#balance').val();

    $('#count').keyup(function(){
        if (parseInt($(this).val()) == NaN){
            error('Введите число');
            $(this).val(0);
        }else{
            var sum =parseInt($(this).val()) * price;
            if (sum > balance){
                error('Недостаточно денег на балансе');
            }else{
                clearError()
                $('#sum').text(sum);
            }
        }
    })
})

function error(text){
    clearError();
    var error = '<div id="rvalidation" class="alert alert-danger" role="alert">' + text + '</div>';
    $('form').prepend(error);
    $('#send').attr('disabled','disabled')
}

function clearError() {
    $('#rvalidation').remove();

    $('#send').removeAttr('disabled')
}
//# sourceMappingURL=buystocks.js.map
