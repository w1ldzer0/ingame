<?php

use Illuminate\Database\Seeder;

class Settings extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('settings')->insert([
            'name'      => 'email',
            'value'     => 'mail@w1ldzer0.ru'
        ]);
    }
}
