<?php

use Illuminate\Database\Seeder;

class UserCreate extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name'      => 'Sergey Gorbunov',
            'email'     => 'mail@w1ldzer0.ru',
            'password'  => bcrypt('dj17e275'),
            'is_admin'  => true
        ]);
    }
}
