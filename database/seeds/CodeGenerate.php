<?php

use Illuminate\Database\Seeder;

class CodeGenerate extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Model\RegCode::class, 500)->create();
    }
}
