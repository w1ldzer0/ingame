<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

# Логин/регистрация
Route::group(['middleware' => 'guest'], function () {
    Route::get('/', 'Auth\AuthController@getLogin');

    Route::get('/login', 'Auth\AuthController@getLogin');
    Route::post('/login', 'Auth\AuthController@postLogin');

    Route::get('/reg', 'Auth\RegisterController@getRegister');
    Route::post('/reg', 'Auth\RegisterController@postRegister');
});

# Для авторизованных пользователей
Route::group(['middleware' => 'auth'], function () {

    Route::get('getFile', 'Controller@getFile');

    Route::group([  'middleware' => 'admin', 'prefix'  => 'admin'], function () {

        Route::get('settings', 'AdminController@getSettings')->name('admin/settings');
        Route::post('settings', 'AdminController@postSettings')->name('admin/settings');
        Route::post('uploadFile/{id}', 'AdminController@uploadFile');
        Route::get('teachers', 'AdminController@teachers');
        Route::post('teachers', 'AdminController@postTeachers');

        Route::get('userStudy/{id}', 'AdminController@getUserStudy')->name('admin/userStudy');
        Route::post('userStudy/{id}', 'AdminController@postUserStudy');

        Route::get('/', 'AdminController@index');
        Route::group(['prefix' => 'regcode'], function() {
            Route::get('delete/{id}', 'AdminController@deleteCode');
            Route::get('list', 'AdminController@regCodeList');
        });

        Route::group(['prefix' => 'stock'], function() {
            Route::get('add/{id?}', 'AdminController@getStock');
            Route::post('add/{id?}', 'AdminController@postStock');
            Route::get('list', 'AdminController@listStock');
        });

        Route::group(['prefix' => 'schedule'], function() {
            Route::get('create/{id?}', 'AdminController@createSchedule');
            Route::post('create/{id?}', 'AdminController@postCreateSchedule');
            Route::get('list', 'AdminController@scheduleList');
        });

        Route::group(['prefix' => 'users'], function(){
            Route::get('list', 'AdminController@getUsers')->name('admin/getUsers');
            Route::get('edit/{id}', 'AdminController@editUser')->name('admin/editUser');
            Route::post('edit/{id}', 'AdminController@postEditUser')->name('admin/editUser');
        });
        Route::group(['prefix' => 'studies'], function (){
            Route::get('list', 'AdminController@getStudiesList')->name('admin/studiesList');
            Route::get('create/{id?}', 'AdminController@getStudy')->name('admin/study');
            Route::post('create/{id?}', 'AdminController@postStudy');
            Route::get('delete/{id}', 'AdminController@deleteStudy');
        });

        Route::group(['prefix' => 'packets'], function (){
            Route::get('list', 'AdminController@getPacketsList')->name('admin/packetsList');
            Route::get('create/{id?}', 'AdminController@getPacket')->name('admin/packet');
            Route::post('create/{id?}', 'AdminController@postPacket');
            Route::get('delete/{id}', 'AdminController@deletePacket');
            Route::get('fill/{id}', 'AdminController@fillPackets');
            Route::post('fill/{id}', 'AdminController@postFillPackets');
        });

        Route::group(['prefix'  => 'depositType'], function () {
            Route::get('create/{id?}', 'AdminController@depositTypeCreate');
            Route::post('create/{id?}', 'AdminController@postDepositTypeCreate');
            Route::get('list', 'AdminController@depositTypesList');
        });
    });

    Route::group(['middleware' => 'user', 'prefix'  => 'home'], function () {
        Route::get('', 'HomeController@index');
        Route::get('schedule/detail/{id}', 'HomeController@detailSchedule');
        Route::post('uploadHomeWork', 'HomeController@uploadHomeWork');
        Route::group(['prefix'  => 'deposit'], function () {
            Route::get('mydeposit', 'HomeController@getDeposit');
            Route::post('create', 'HomeController@createDeposit');
            Route::get('create/{id}', 'HomeController@getCreateDeposit');
            Route::get('list', 'HomeController@getDepositTypesList');
        });

        Route::group(['prefix'  => 'stocks'], function () {
            Route::get('list', 'HomeController@stocksList');
            Route::get('buy/{id}', 'HomeController@getStocksBuy');
            Route::post('buy/{id}', 'HomeController@postStocksBuy');
            Route::get('my', 'HomeController@myStocks');
            Route::get('sell/{id}', 'HomeController@getSellStock');
        });

        Route::get('schedule/{month?}', 'HomeController@schedule');

        Route::get('calendar', 'HomeController@calendar')->name('home/calendar');

        Route::get('send', 'HomeController@getSendMessage')->name('home/send');
        Route::post('send', 'HomeController@postSendMessage');
    });

    # Логаут
    Route::get('logout', 'Auth\AuthController@logout');
});
