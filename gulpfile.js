const elixir = require('laravel-elixir');

require('laravel-elixir-vue-2');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(mix => {
    mix.sass('app.scss')
       .webpack('app.js');

    mix.scripts([
        'jquery-3.1.1.min.js',
        '../../../node_modules/bootstrap-sass/assets/javascripts/bootstrap.min.js',
        '../../../node_modules/bootstrap-sass/assets/javascripts/bootstrap/collapse.js',
        '../../../node_modules/bootstrap-sass/assets/javascripts/bootstrap/transition.js',
        'moment.min.js',
        'bootstrap-datetime-picker.min.js',
        'modules/reg.js',
        'modules/fill.js'
    ])

    mix.scripts([
        '../../../bower_components/underscore/underscore.js',
        //'bower_components/bootstrap/dist/js/bootstrap.js',
        '../../../bower_components/moment/moment.js',
        '../../../bower_components/bootstrap-calendar/js/calendar.js',
    ], 'public/js/calendar.js')
    mix.scripts([
        'buystocks.js'
    ], 'public/js/buystocks.js')
    mix.scripts([
        '../../../bower_components/bootstrap-fileinput/js/fileinput.min.js',
        '../../../bower_components/bootstrap-fileinput/js/locales/ru.js',
        'fileUpload.js'
    ], 'public/js/fileinput.js')

    mix.copy('bower_components/bootstrap-fileinput/css/fileinput.min.css', 'public/css/fileinput.css');
    mix.copy('bower_components/bootstrap-calendar/css/calendar.css', 'public/css/calendar.css');

    mix.scriptsIn('node_modules/bootstrap-sass/assets/javascripts/bootstrap/', 'public/js/bootstrap.module.js');

    mix.version('js/calendar.js')
});
