<?php

namespace App\Libraries;
use App\Model\Settings as SettingsModel;
class Settings
{
    private $instance = null;
    private $settings = [];

    public function __construct()
    {
        $this->settings = SettingsModel::getAll();
    }

    public function get($key)
    {
        if (isset($this->settings[$key])){
            return $this->settings[$key];
        }else{
            return null;
        }
    }
}