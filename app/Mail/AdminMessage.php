<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Facades\Settings;
use Illuminate\Support\Facades\Auth;

class AdminMessage extends Mailable
{
    use Queueable, SerializesModels;
    public $user = null;
    public $text = '';
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($text)
    {
        $this->user = Auth::user();
        $this->text = $text;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->to(Settings::get('email'))->from(Settings::get('email'))->view('emails.message');
    }
}
