<?php

namespace App\Mail;

use App\Model\Schedule;
use Illuminate\Bus\Queueable;
use Illuminate\Http\UploadedFile;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Facades\Settings;
use Illuminate\Support\Facades\Auth;

class HomeWorkMessage extends Mailable
{
    use Queueable, SerializesModels;
    public $user = null;
    public $text = '';
    /**
     * @var UploadedFile|null
     */
    public $file = null;
    public $schedule = null;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Schedule $schedule, UploadedFile $file)
    {
        $this->user = Auth::user();
        $this->schedule = $schedule;
        $this->file = $file;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
            ->to(Settings::get('email'))
            ->from(Settings::get('email'))
            ->attach($this->file->getPath(),['as' => $this->file->getClientOriginalName()])
            ->view('emails.homework');
    }
}
