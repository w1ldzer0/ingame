<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Libraries\Settings;
/**
* Class SettingsProvider
* @package App\Providers
*/
class SettingServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->singleton('Settings', function ($app) {
            return new Settings();
        });
    }

    public function boot()
    {
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return ['Settings'];
    }
}