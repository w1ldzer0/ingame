<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Validator;
use Illuminate\Support\Facades\Auth;
/**
* Class ValidatorProvider
* @package App\Providers
*/
class ValidatorServiceProvider extends ServiceProvider
{
    public function register()
    {
        // TODO: Implement register() method.
    }

    public function boot()
    {
        Validator::extend('canPay', function($attribute, $value, $parameters, $validator) {
            $balance = Auth::user()->balance;
            if ((float)$value <= (float)$balance){
                return true;
            }

            return false;
        });
    }
}