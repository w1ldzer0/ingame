<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Study extends  Model
{
    //protected $with = ['schedule'];
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title'
    ];

    public function schedule()
    {
        return $this->hasMany('App\Model\Schedule','study_id', 'id');
    }
}