<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class StockHistory extends  Model
{
    protected $table = 'stock_history';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'company_id', 'price'
    ];

    public function company()
    {
        return $this->belongsTo('App\Model\Company', 'company_id', 'id');
    }
}
