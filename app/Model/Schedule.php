<?php

namespace App\Model;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
class Schedule extends Model
{
    protected $with = ['study'];
    protected $table = 'schedule';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'study_id', 'age', 'date', 'webinar_record'
    ];

    public function study()
    {
        return $this->hasOne('App\Model\Study', 'id', 'study_id');
    }

    public function getFormatedDate(string $format = 'd-m-Y h:m'): string
    {
        return date($format, strtotime($this->date));
    }

    public function getFiles()
    {
        $id = $this->id;
        $files = Storage::files('homework/'.$id);
        $return = [];
        foreach ($files as $file){
            $return[] = [
                'name'  => basename($file),
                'url'   => '/getFile?file='.$file
                ];
        }
        return $return;
    }

}
