<?php

namespace App\Model;


use Illuminate\Database\Eloquent\Model;

class Settings extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'value'
    ];

    public $timestamps = false;

    public static function getAll()
    {
        $settings = self::all();
        $return = [];
        foreach ($settings as $setting)
        {
            $return[$setting['name']] = $setting['value'];
        }
        return $return;
    }

}
