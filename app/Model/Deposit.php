<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Deposit extends  Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'start_date', 'finish_date', 'amount', 'type', 'tax'
    ];

    public function depositType()
    {
        return $this->belongsTo('App\Model\DepositType', 'type', 'id');
    }
}
