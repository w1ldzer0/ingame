<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Packet extends Model
{

    protected $with = ['studies'];
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'describe', 'price'
    ];

    public function studies()
    {
        return $this->hasMany('App\Model\StudyInPacket', 'packet_id', 'id');
    }

    public function users()
    {
        return $this->belongsTo('App\Model\User');
    }
}
