<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class CompanyStock extends  Model
{
    protected $table = 'CompanyStock';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'price', 'min_price'
    ];

    public function history()
    {
        return $this->hasMany('App\Model\StockHistory', 'company_id', 'id');
    }

}
