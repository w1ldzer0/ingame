<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class StudyInPacket extends  Model
{
    protected $with = ['study'];

    protected $table = 'studies_in_packets';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'study_id', 'packet_id'
    ];

    public function study()
    {
        return $this->hasOne('App\Model\Study', 'id');
    }


}