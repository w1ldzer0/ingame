<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class UserStock extends  Model
{
    public $with = ['stock','user'];
    protected $fillable = [
        'user_id', 'stock_id', 'price', 'cnt'
    ];

    public function user()
    {
        return $this->hasOne('App\Model\User', 'id', 'user_id');
    }

    public function stock()
    {
        return $this->hasOne('App\Model\CompanyStock', 'id', 'stock_id');
    }
}