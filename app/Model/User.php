<?php

namespace App\Model;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'is_admin', 'balance', 'packet_id', 'first_name', 'last_name'
    ];

    protected $with = ['packet', 'schedule'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function isAdmin()
    {
        return $this->is_admin;
    }

    public function decreaseMoney($amount)
    {
        $this->balance -= $amount;
    }

    public function packet()
    {
        return $this->hasOne('App\Model\Packet', 'id', 'packet_id');
    }

    public function schedule()
    {
        return $this->hasMany('App\Model\UserSchedule', 'user_id', 'id');
    }



    public function getMySchedulesId()
    {
        $scheduleStudies = [];
        foreach ($this->schedule as $schedule){
            $scheduleStudies[$schedule->schedule_id] = $schedule->schedule_id;
        }

        return $scheduleStudies;
    }

}
