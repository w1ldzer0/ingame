<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class UserPacket extends  Model
{

    protected $table = 'user_packet';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'packet_id'
    ];


}