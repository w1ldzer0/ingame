<?php

namespace App\Model;


use Illuminate\Database\Eloquent\Model;

class UserSchedule extends Model
{
    //protected $with = ['schedule'];
    protected $table = 'user_schedule';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'schedule_id'
    ];

    public function schedule()
    {
        return $this->belongsTo('App\Model\Schedule', 'schedule_id', 'id');
    }

    public function user()
    {
        return $this->belongsTo('App\Model\User', 'id', 'user_id');
    }

}