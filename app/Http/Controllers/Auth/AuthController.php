<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Validation\User\UserLoginRequest;
use Illuminate\Support\Facades\Auth;
use App\Model\User;
use Illuminate\Cache\RateLimiter;
use Illuminate\Support\Facades\View;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class AuthController extends Controller
{
    /**
     * Максимальное количество попыток входа
     */
    const MAX_LOGIN_ATTEMPTS = 4;

    /**
     * @var RateLimiter
     */
    protected $rateLimiter;

    /**
     * Create a new authentication controller instance.
     *
     * @param RateLimiter $rateLimiter
     */
    public function __construct(RateLimiter $rateLimiter)
    {
        $this->rateLimiter = $rateLimiter;
    }

    /**
     * Вход пользователя.
     * Использует RateLimiter, ограничивая число попыток входа
     *
     * @param UserLoginRequest $loginRequest
     * @return Response
     */
    public function postLogin(UserLoginRequest $loginRequest)
    {
        // Если превышено количество попыток входа,
        // то мы не будем пытаться авторизовать пользователя.
        if ($this->hasTooManyLoginAttempts($loginRequest)) {
            $responseText = trans('auth.throttle', [
                'seconds' => $this->secondsRemainingOnLockout($loginRequest)
            ]);

            return redirect('login')->with([
                'error' => $responseText
            ]);
        }

        $authStatus = Auth::attempt([
            'email' => $loginRequest->get('email'),
            'password' => $loginRequest->get('password')
        ], $loginRequest->get('remember_me') != 'false');

        if (!$authStatus) {
            $responseText = trans('auth.failed');
            $this->addAttempt($loginRequest);
            return redirect('login')->with('error', $responseText)->withInput();
        }

        if (Auth::user()->isAdmin()){
            return redirect('admin');
        }else{
            return redirect('home');
        }
    }

    public function getLogin()
    {
        return View::make('common/login');
    }


    /**
     * Определяет, не было ли превышено максимально количество попыток входа
     *
     * @param Request $request
     * @return bool
     */
    private function hasTooManyLoginAttempts(Request $request) : bool
    {
        /** @var RateLimiter $rateLimiter */
        $rateLimiter = app()->make(RateLimiter::class);
        return $rateLimiter->tooManyAttempts($this->getKey($request), static::MAX_LOGIN_ATTEMPTS);
    }

    /**
     * Возвращает количество секунд, через снова можно будет совершить попытку входа
     *
     * @param $loginRequest
     * @return int
     */
    private function secondsRemainingOnLockout($loginRequest) : int
    {
        return $this->rateLimiter->availableIn($this->getKey($loginRequest));
    }

    /**
     * Возвращает api
     *
     * @param Request $request
     * @return mixed
     */
    private function getKey(Request $request)
    {
        return $request->get($request->get('email') . $request->ip());
    }

    /**
     * Увеличивает количествол попыток входа
     *
     * @param $loginRequest
     */
    private function addAttempt($loginRequest)
    {
        $this->rateLimiter->hit($this->getKey($loginRequest));
    }
}