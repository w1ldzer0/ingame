<?php

namespace App\Http\Controllers\Auth;

use App\Http\Requests\Validation\User\UserRegisterRequest;
use App\Model\User;
use Illuminate\Support\Facades\Auth;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function getRegister()
    {
        return View::make('common/reg');
    }

    public function postRegister(UserRegisterRequest $request)
    {
        $this->create([
                'first_name'    => $request->get('first_name'),
                'last_name'     => $request->get('last_name'),
                'email'         => $request->get('email'),
                'birthday'      => strtotime($request->get('birthday')),
                'password'      => $request->get('password')
            ]
        );

        Auth::attempt([
            'email' => $request->get('email'),
            'password' => $request->get('password')
        ]);
        return redirect('/home');
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        return User::create([
            'first_name'   => $data['first_name'],
            'last_name'    => $data['last_name'],
            'email'         => $data['email'],
            'birthday'      => $data['birthday'],
            'password'      => bcrypt($data['password']),
        ]);
    }
}

