<?php

namespace App\Http\Controllers;

//use App\Http\Middleware\User;
use App\Http\Requests\Validation\DepositType\DepositTypeRequest;
use App\Http\Requests\Validation\Schedule\ScheduleCreateRequest;
use App\Http\Requests\Validation\Settings\SettingsRequest;
use App\Http\Requests\Validation\Stocks\CompanyRequest;
use App\Http\Requests\Validation\Studies\PacketRequest;
use App\Http\Requests\Validation\Studies\PacketStudiesRequest;
use App\Http\Requests\Validation\Studies\StudiesRequest;
use App\Http\Requests\Validation\Studies\TeachersRequest;
use App\Http\Requests\Validation\Studies\UserStudiesRequest;
use App\Http\Requests\Validation\User\UserEditRequest;
use App\Http\Requests\Validation\FileRequest;
use App\Model\{
    CompanyStock, Packet, Schedule, StockHistory, StudyInPacket, RegCode, DepositType, Settings, Study, Packets, User, UserSchedule
};


use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;

use Illuminate\Support\Facades\View;

class AdminController extends Controller
{
    public function index()
    {
        return View::make('admin/dashboard');
    }

    public function getUsers()
    {
        $users = User::where('is_admin', '=', false)->paginate(30);
        return View::make('admin/Users/List', [
            'users' => $users
        ]);
    }

    public function editUser(int $userId)
    {
        $user = User::find($userId);

        if (!$user) {
            return redirect('admin/users/list')
                ->with('error', 'Пользователь не найден');
        }
        $packets = Packet::all();
        return View::make('admin/Users/Edit', [
            'user'  => $user,
            'packets'   => $packets
        ]);
    }

    public function postEditUser(UserEditRequest $request, int $userId)
    {
        $user = User::find($userId);

        if (!$user) {
            return back()
                ->with('error', 'Пользователь не найден');
        }

        $user->first_name = $request->get('first_name');
        $user->last_name = $request->get('last_name');
        $user->email = $request->get('email');
        $user->balance = $request->get('balance');
        $user->packet_id = $request->get('packet');
        $user->birthdate = date(DATE_ATOM,strtotime($request->get('birthday')));

        $user->save();

        return redirect('admin/users/list')->with('succes', 'Данные пользователя успешно обновлены');
    }

    public function depositTypeCreate($id = null)
    {
        $data = [];
        if ($id) {
            $depositType = DepositType::find($id);
            if ($depositType) {
                $data['depositType'] = $depositType;
            }

        }
        return View::make('admin/Deposits/DepositTypeCreate', $data);
    }

    public function postDepositTypeCreate(DepositTypeRequest $request, $id = null)
    {
        if ($id && $depositType = DepositType::find($id)) {
            $depositType['title'] = $request->get('title');
            $depositType['period'] = $request->get('period');
            $depositType['tax'] = $request->get('tax');
            $depositType->save();
        } else {
            DepositType::create(
                [
                    'title' => $request->get('title'),
                    'period' => $request->get('period'),
                    'tax' => $request->get('tax')
                ]
            );
        }

        return redirect('admin/depositType/list');
    }

    public function depositTypesList()
    {
        $deposits = DepositType::orderBy('id')->paginate(30);
        return View::make('admin/Deposits/DepositTypesList', [
            'deposits' => $deposits
        ]);
    }

    public function getSettings()
    {
        $settings = Settings::getAll();
        return View::make('admin/settings', [
            'settings' => $settings
        ]);
    }

    public function postSettings(SettingsRequest $request)
    {
        //$data = $request->toArray();
        foreach ($request->all() as $key => $value) {
            $settingModel = Settings::where('name', '=', $key)->first();
            if ($settingModel) {
                $settingModel->name = $key;
                $settingModel->value = $value;
                $settingModel->save();
            }else{
                $settingModel = new Settings([
                    'name'  => $key,
                    'value' => $value
                ]);
                $settingModel->save();
            }

        }
        return redirect(route('admin/settings'))
            ->with('success', 'Настройки обновлены');
    }


    public function regCodeList()
    {
        $codes = RegCode::orderBy('id')->paginate(30);
        return View::make('admin/regCodeList', [
            'codes' => $codes
        ]);
    }

    public function deleteCode($id)
    {
        $regCode = RegCode::find($id);
        if (!$regCode) {
            return redirect('admin/codeList')
                ->with('error', 'Код не найден');
        }

        $regCode->delete();

        return redirect('admin/codeList')
            ->with('success', 'Успешно удален');
    }

    public function getStudy($id = null)
    {
        $data = [];
        if ($id) {
            $data['study'] = Study::find($id);
        }

        return View::make('admin/Studies/Create', $data);
    }

    public function postStudy(StudiesRequest $request, $id = null)
    {
        if ($id){
            $study = Study::find($id);
            if (!$study){
                return redirect('admin/studies/create')
                    ->with('error', 'Занятие не найдено');
            }

            $study->title = $request->get('title');
            $study->webinar_url = $request->get('url');
            $study->save();
            return redirect('admin/studies/list')
                ->with('success', 'Занятие успешно изменено');
        }else{
            $study = new Study([
                'title' => $request->get('title'),
                'webinar_url'   => $request->get('url')
            ]);
            $study->save();
            return redirect('admin/studies/list')
                ->with('success', 'Занятие успешно создано');
        }
    }

    public function getStudiesList()
    {
        $studies = Study::paginate(30);
        return View::make('admin/Studies/List', [
            'studies'   => $studies
        ]);
    }

    public function deleteStudy($id)
    {
        $study = Study::find($id);
        if (!$study) {
            return redirect('admin/studies/list')
                ->with('error', 'Задание не найдено');
        }

        $study->delete();

        return redirect('admin/studies/list')
            ->with('success', 'Успешно удален');
    }


    public function getPacket($id = null)
    {
        $data = [];
        if ($id) {
            $data['packet'] = Packet::find($id);
        }

        return View::make('admin/Packets/Create', $data);
    }

    public function postPacket(PacketRequest $request, $id = null)
    {
        if ($id){
            $packet = Packet::find($id);
            if (!$packet){
                return redirect('admin/packets/create')
                    ->with('error', 'Пакет не найдено');
            }

            $packet->title = $request->get('title');
            $packet->describe = $request->get('describe');
            $packet->title = $request->get('price');
            $packet->save();
            return redirect('admin/packets/list')
                ->with('success', 'Пакет успешно изменено');
        }else{
            $packets = new Packet([
                'title' => $request->get('title'),
                'describe' => $request->get('describe'),
                'price' => $request->get('price')
            ]);
            $packets->save();
            return redirect('admin/packets/list')
                ->with('success', 'Пакет успешно создано');
        }
    }

    public function getPacketsList()
    {
        $packets = Packet::paginate(30);
        return View::make('admin/Packets/List', [
            'packets'   => $packets
        ]);
    }

    public function deletePacket($id)
    {
        $packet = Packet::find($id);
        if (!$packet) {
            return redirect('admin/packets/list')
                ->with('error', 'Пакет не найден');
        }

        $packet->delete();

        return redirect('admin/packets/list')
            ->with('success', 'Успешно удален');
    }

    public function fillPackets(int $id)
    {
        $data = [];
        $data['packet'] = Packet::find($id);
        $data['studies'] = Study::all();
        return View::make('admin/Packets/Fill', $data);
    }

    public function postFillPackets(PacketStudiesRequest $request, int $id = null)
    {
        if ($id){
            $packet = Packet::find($id);
            if (!$packet){
                return redirect('admin/packets/create')
                    ->with('error', 'Пакет не найден');
            }

            $studies = [];
            foreach ($request->get('studies') as $studyId){
                $studies[] = [
                    'packet_id' => $id,
                    'study_id'  => $studyId
                ];
            }
            StudyInPacket::insert($studies);
            return redirect('admin/packets/list')
                ->with('success', 'Пакет успешно изменен');
        }
    }

    public function createSchedule($id = null)
    {
        $studies = Study::all();

        $data = [
            'studies'    => $studies
        ];
        if ($id){
            $schedule = Schedule::find($id);
            $data['schedule'] = $schedule;
        }
        return View::make('admin/Schedule/Create', $data);
    }

    public function postCreateSchedule(ScheduleCreateRequest $request, $id = null)
    {
        if ($id){
            $schedule = Schedule::find($id);
        } else {
            $schedule = new Schedule();
        }
        $data = [
            'study_id' => $request->get('study'),
            'age'   => $request->get('age'),
            'date'  => date(DATE_ATOM, strtotime($request->get('date'))),
            'webinar_record'   => $request->get('url')
        ];

        foreach ($data as $key=>$value){
            $schedule->$key = $value;
        }
        $schedule->saveOrFail();

        return redirect('admin/schedule/list')->with([
            'success'   =>  'Занятие успешно добавлено в расписание'
        ]);
    }

    public function scheduleList()
    {
        $studies = Schedule::orderBy('date', 'desc')->paginate(30);
        $studies[0]->getFiles();
        return View::make('admin/Schedule/List', [
            'studies'   => $studies
        ]);
    }

    public function getUserStudy($id)
    {
        $user = User::find($id);

        return View::make('admin/Users/Studies',[
            'user'      => $user,
        ]);
    }

    public function postUserStudy(UserStudiesRequest $request, int $id)
    {
        $userStudies = UserSchedule::where('user_id', '=', $id)->delete();
        $schedule = [];
        if ($request->get('studies')) {
            foreach ($request->get('studies') as $row) {
                $schedule[] = [
                    'schedule_id' => $row,
                    'user_id' => $id
                ];
            }

            UserSchedule::insert($schedule);
        }
        return back()->with([
            'success'   =>  'Занятие успешно добавлено в расписание'
        ]);
    }

    public function getStock($id = null)
    {
        $company = null;
        if ($id){
            $company = CompanyStock::find($id);
        }

        return View::make('admin/Stock/Add',[
            'company'   => $company
        ]);
    }

    public function postStock(CompanyRequest $request, $id = null)
    {
        if ($id){
            $company = CompanyStock::find($id);
        } else {
            $company = new CompanyStock();
        }
        $data = [
            'title' => $request->get('title'),
            'price' => $request->get('price'),
            'min_price' => $request->get('min_price')
        ];
        foreach ($data as $key => $value){
            $company->$key = $value;
        }
        $company->save($data);
        $historyData = [
            'company_id'    => $id,
            'price'         => $request->get('price')
        ];
        $historyStock = new StockHistory($historyData);
        $company->history()->save($historyStock);

        return redirect('admin/stock/list')->with([
            'success'   =>  'Занятие успешно добавлено в расписание'
        ]);
    }

    public function listStock()
    {
        $companies = CompanyStock::paginate();

        return View::make('admin/Stock/List', [
            'companies' => $companies
        ]);
    }

    public function uploadFile(FileRequest $request, $id)
    {
        foreach ($request->files as $files){
            foreach ($files as $file){
                Storage::disk('local')->put('homework/'.$id.'/'.$file->getClientOriginalName(),  File::get($file));
            }

        }
        return response()->json([
            'success'   => 1
        ]);
    }

    public function teachers()
    {
        $studies = Study::orderBy('id', 'desc')->get();
        return View::make('admin/Teachers',[
            'studies'   => $studies
        ]);
    }

    public function postTeachers(TeachersRequest $request)
    {
        foreach ($request->get('email') as $studyId => $email){
            $study = Study::find($studyId);
            $study->teacher_email = $email;
            $study->save();
        }

        return redirect('admin/teachers')->with([
            'success'   =>  'Email изменены'
        ]);
    }

}