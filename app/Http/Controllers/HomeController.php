<?php

namespace App\Http\Controllers;

use App\Http\Requests\Validation\Deposit\DepositRequest;
use App\Http\Requests\Validation\FileRequest;
use App\Http\Requests\Validation\Stocks\BuyRequest;
use App\Mail\AdminMessage;
use App\Mail\HomeWorkMessage;
use App\Model\CompanyStock;
use App\Model\DepositType;
use App\Model\Schedule;
use App\Model\Study;
use App\Model\UserSchedule;
use App\Model\UserStock;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;
use App\Facades\Settings;
use App\Model\Deposit;
use Illuminate\Support\Facades\Mail;
use Auth;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return View::make('home/index');
    }

    public function getDeposit()
    {
        $userId = Auth::user()->id;
        $deposits = Deposit::where('user_id', '=', $userId)
            ->paginate(30);
        return View::make('home/Deposits/MyDeposits', [
            'deposits'   => $deposits
        ]);
    }

    public function getCreateDeposit($typeId)
    {
        $depositType = DepositType::find($typeId);
        // Если превышено количество попыток входа,
        // то мы не будем пытаться авторизовать пользователя.
        if (!$depositType) {
            $responseText = 'Депозит не найден';
            return redirect('/home/deposit/list')->with([
                'error' => $responseText
            ]);
        }

        return View::make('home/Deposits/CreateDeposit', [
            'depositType'  => $depositType
        ]);
    }

    public function getDepositTypesList()
    {
        $deposits = DepositType::orderBy('id')->paginate(30);
        return View::make('home/Deposits/TypesList', [
            'deposits'  => $deposits
        ]);
    }

    public function createDeposit(DepositRequest $request)
    {
        /**
         * @var $user \App\Model\User
         */
        $user = Auth::user();
        $userId = $user->id;
        $user->decreaseMoney ($request->get('amount'));
        $user->save();
        $depositType = DepositType::find($request->get('type'));
        Deposit::create(
            [
                'user_id'       => $userId,
                'start_date'    => date(DATE_RSS),
                'finish_date'   => date(DATE_RSS, time() + $depositType->period*86400),
                'amount'        => $request->get('amount'),
                'type'          => $request->get('type'),
                'tax'           => $depositType->tax
            ]
        );

        return redirect('/home/deposit/mydeposit')->with('success','Вклад открыт');
    }

    public function getSendMessage()
    {

        return View::make('home/send');
    }

    public function postSendMessage(Request $request)
    {
        $text = $request->get('message');
        Mail::send(new AdminMessage($text));
        return redirect(route('home/send'))->with('success', 'Ваше сообщение доставлено');
    }

    public function calendar()
    {
        $userData = Auth::user();

        $schedule = UserSchedule::where('user_id','=', $userData->id);
        return View::make('home/Calendar/Index', [
            'schedule'  => $schedule
        ]);
    }

    public function schedule($month = null)
    {
        $userData = Auth::user();
        $schedule = UserSchedule::where('user_id','=', $userData->id)->get();

        $return = [];
        foreach ($schedule as $key => $study) {
            if ($study->schedule->webinar_record){
                $url = $study->schedule->webinar_record;
                $record = true;
            }else{
                $url = $study->schedule->study->webinar_url;
                $record = false;
            }
            $return[] = [
                'title' => $study->schedule->study->title.' '.date('h:m',strtotime($study->schedule->date)),
                'url' =>    '/home/schedule/detail/'.$study->schedule->id,
                'record'    => $record,
                'start' => strtotime($study->schedule->date) . '000',
                'end' => strtotime($study->schedule->date) . '000'
            ];
        }

        return response()->json([
            'success'   => 1,
            'result'    => $return
        ]);


    }

    public function detailSchedule($id){
        $schedule = Schedule::find($id);
        return View::make('home/Schedule/Detail',[
            'schedule'  => $schedule
        ]);
    }

    public function uploadHomeWork(FileRequest $request)
    {
        $scheduleId = $request->get('schedule_id');
        $schedule = Schedule::find($scheduleId);
        Mail::send(new HomeWorkMessage($schedule, $request->file('file')));
        return back()->with('success', 'Ваше сообщение доставлено');
    }

    public function stocksList()
    {
        $stocks = CompanyStock::paginate(30);

        return View::make('home/Stock/List',[
            'stocks'    => $stocks
        ]);
    }

    public function getStocksBuy($id)
    {
        $stock = CompanyStock::find($id);
        return View::make('home/Stock/Buy', [
            'stock' => $stock,
            'user'  => Auth::user()
        ]);
    }

    public function postStocksBuy(BuyRequest $request, int $id)
    {
        $stock = CompanyStock::find($id);
        if (!$stock){
            return back()
                ->with('error', 'Акции не найдены');
        }
        if ($stock->price * $request->get('count') > Auth::user()->balance){
            return back()
                ->with('error', 'Недостаточно средств');
        }
        DB::beginTransaction();
        try {
            $user =  Auth::user();
            $user->balance = $user->balance - $stock->price * $request->get('count');
            $user->saveOrFail();

            $userStock = new UserStock([
                'user_id'   => $user->id,
                'stock_id'  => $id,
                'price'     => $stock->price,
                'cnt'       => $request->get('count')
            ]);
            $userStock->saveOrFail();
        } catch(\Exception $e) {
            dd($e->getMessage());
            DB::rollback();
            return back()
                ->with('error', 'Небольшая проблема');
        }
        DB::commit();
        return back()
            ->with('success', 'Акции куплены');
    }

    public function getSellStock($id)
    {
        $userStock = UserStock::find($id);
        if (!$userStock){
            return back()
                ->with('error', 'Акции не найдены');
        }

        $user = Auth::user();

        DB::beginTransaction();
        try {
            $currentPrice = $userStock->stock->price;

            $sum = $currentPrice * $userStock->cnt;
            $user->balance += $sum;

            $userStock->selled = true;
            $user->save();
            $userStock->save();
        } catch (\Exception $e){
            DB::rollback();
            dd($e->getMessage());
            return back()
                ->with('error', 'Небольшая проблема');
        }
        DB::commit();
        return back()
            ->with('success', 'Акции проданы');
    }

    public function myStocks()
    {
        $stocks = UserStock::where('user_id', '=', Auth::user()->id)
            ->where('selled', '=', false)
            ->paginate(30);
        return View::make('home/Stock/MyList',[
            'stocks'    => $stocks
        ]);

    }
}
