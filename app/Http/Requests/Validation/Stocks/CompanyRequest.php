<?php

namespace App\Http\Requests\Validation\Stocks;

use App\Http\Requests\ValidationRequest;

class CompanyRequest extends ValidationRequest
{
    public function rules(): array
    {
        return [
            'title' => 'required|string',
            'price' => 'required|numeric|min:0',
            'min_price' => 'required|numeric|min:0',
        ];
    }
}