<?php

namespace App\Http\Requests\Validation\Stocks;

use App\Http\Requests\ValidationRequest;

class BuyRequest extends ValidationRequest
{
    public function rules(): array
    {
        return [
            'count' => 'required|numeric|min:1'
        ];
    }
}