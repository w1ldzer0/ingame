<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 22.08.16
 * Time: 15:47
 */

namespace App\Http\Requests\Validation\DepositType;


use App\Http\Requests\ValidationRequest;

/**
 * Class DepositTypeRequest
 * @package App\Http\Requests\Validation\DepositType
 */
class DepositTypeRequest extends ValidationRequest
{
    /**
     * @inheritdoc
     */
    public function rules(): array
    {
        return [
            'title'     => 'required',
            'period'    => 'required|integer',
            'tax'       => 'required|integer|min:1',
        ];
    }

}