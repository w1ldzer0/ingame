<?php
namespace App\Http\Requests\Validation;

use App\Http\Requests\ValidationRequest;

class FileRequest extends \App\Http\Requests\ValidationRequest
{
    public function rules():array
    {
        return [
            'files.*' => 'required|file'
        ];
    }
}