<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 22.08.16
 * Time: 15:47
 */

namespace App\Http\Requests\Validation\Settings;


use App\Http\Requests\ValidationRequest;

/**
 * Class DepositTypeRequest
 * @package App\Http\Requests\Validation\Settings
 */
class SettingsRequest extends ValidationRequest
{
    /**
     * @inheritdoc
     */
    public function rules(): array
    {
        return [
            'email'     => 'required|email',
        ];
    }

}