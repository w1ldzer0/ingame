<?php
namespace App\Http\Requests\Validation\Schedule;


use App\Http\Requests\ValidationRequest;


class ScheduleCreateRequest extends ValidationRequest
{
    public function rules(): array
    {
        return [
            'study' =>  'required|exists:studies,id',
            'ages'  =>  'string',
            'url'   =>  'required|url',
            'date'  =>  ''
        ];
    }
}