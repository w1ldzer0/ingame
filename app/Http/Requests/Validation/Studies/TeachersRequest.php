<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 22.08.16
 * Time: 15:47
 */

namespace App\Http\Requests\Validation\Studies;


use App\Http\Requests\ValidationRequest;

/**
 * Class StudiesRequest
 * @package App\Http\Requests\Validation\Studies
 */
class TeachersRequest extends ValidationRequest
{
    /**
     * @inheritdoc
     */
    public function rules(): array
    {
        return [
            'email'     => 'array',
            'email.*'   => 'required|email'
        ];
    }

}