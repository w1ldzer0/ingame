<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 22.08.16
 * Time: 15:47
 */

namespace App\Http\Requests\Validation\Studies;


use App\Http\Requests\ValidationRequest;

/**
 * Class PacketStudiesRequest
 * @package App\Http\Requests\Validation\Studies
 */
class PacketStudiesRequest extends ValidationRequest
{
    /**
     * @inheritdoc
     */
    public function rules(): array
    {
        return [
            'ages'      => 'array',
            'ages.*'    => 'string',
            'time'      => 'array',
            'time.*'    => 'required|string',
            'studies'   => 'array',
            'studies.*' => 'required|string',
        ];
    }

}
