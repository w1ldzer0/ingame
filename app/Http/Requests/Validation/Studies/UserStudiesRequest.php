<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 22.08.16
 * Time: 15:47
 */

namespace App\Http\Requests\Validation\Studies;


use App\Http\Requests\ValidationRequest;

/**
 * Class UserStudiesRequest
 * @package App\Http\Requests\Validation\Studies
 */
class UserStudiesRequest extends ValidationRequest
{
    /**
     * @inheritdoc
     */
    public function rules(): array
    {
        return [

        ];
    }

}