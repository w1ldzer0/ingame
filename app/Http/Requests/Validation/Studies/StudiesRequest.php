<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 22.08.16
 * Time: 15:47
 */

namespace App\Http\Requests\Validation\Studies;


use App\Http\Requests\ValidationRequest;

/**
 * Class StudiesRequest
 * @package App\Http\Requests\Validation\Studies
 */
class StudiesRequest extends ValidationRequest
{
    /**
     * @inheritdoc
     */
    public function rules(): array
    {
        return [
            'title'     => 'required|string',
            'url'       => 'required|url'
        ];
    }

}