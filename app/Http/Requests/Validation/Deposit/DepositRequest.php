<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 22.08.16
 * Time: 15:47
 */

namespace App\Http\Requests\Validation\Deposit;


use App\Http\Requests\ValidationRequest;

/**
 * Class DepositRequest
 * @package App\Http\Requests\Validation\Deposit
 */
class DepositRequest extends ValidationRequest
{
    /**
     * @inheritdoc
     */
    public function rules(): array
    {
        return [
            'type'      => 'required|integer|exists:deposit_types,id',
            'amount'    => 'required|integer|canPay',
        ];
    }

}