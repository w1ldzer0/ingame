<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 22.08.16
 * Time: 15:47
 */

namespace App\Http\Requests\Validation\User;


use App\Http\Requests\ValidationRequest;

/**
 * Class UserLoginRequest
 * @package App\Http\Requests\Validation\User
 *
 * Валидация введенных пользователем полей при входе.
 */
class UserRegisterRequest extends ValidationRequest
{
    /**
     * @inheritdoc
     */
    public function rules(): array
    {
        return [
            'first_name'    => 'required|max:255',
            'last_name'     => 'required|max:255',
            'birthday'      => 'required|date_format:d/m/Y',
            'code'          => 'required|exists:reg_code,code',
            'email'         => 'required|email|max:255|unique:users',
            'password'      => 'required|min:6|confirmed',
        ];
    }

}