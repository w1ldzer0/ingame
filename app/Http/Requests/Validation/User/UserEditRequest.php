<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 22.08.16
 * Time: 15:47
 */

namespace App\Http\Requests\Validation\User;


use App\Http\Requests\ValidationRequest;
use Illuminate\Validation\Rule;

/**
 * Class UserEditRequest
 * @package App\Http\Requests\Validation\User
 *
 * Валидация введенных пользователем полей при входе.
 */
class UserEditRequest extends ValidationRequest
{
    /**
     * @inheritdoc
     */
    public function rules(): array
    {
        return [
            'first_name'    => 'required|max:255',
            'last_name'     => 'required|max:255',
            'birthday'      => 'required|date_format:Y-m-d',
            'email'         => ['required','email','max:255',
                //Rule::unique('users')->ignore($this->get('id'))
                ],
            'packet'        => 'required|exists:packets,id',
            'balance'       => 'required|min:0|integer'
        ];
    }

}