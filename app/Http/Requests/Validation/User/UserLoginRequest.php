<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 22.08.16
 * Time: 15:47
 */

namespace App\Http\Requests\Validation\User;


use App\Http\Requests\ValidationRequest;

/**
 * Class UserLoginRequest
 * @package App\Http\Requests\Validation\User
 *
 * Валидация введенных пользователем полей при входе.
 */
class UserLoginRequest extends ValidationRequest
{
    /**
     * @inheritdoc
     */
    public function rules(): array
    {
        return [
            'email' => 'required|email',
            'password' => 'required'
        ];
    }

}