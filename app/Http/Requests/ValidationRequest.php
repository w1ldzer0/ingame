<?php

namespace App\Http\Requests;
use Illuminate\Foundation\Http\FormRequest;

/**
 * Class ValidationRequest
 * @package App\Http\Requests
 *
 * Запрос, который не нужно проверять на права доступа.
 * Т.е. в классе, наследующегося от этого, требуется определить
 * только метод rules.
 */
abstract class ValidationRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    /**
     * Возвращает массив правил для валидатора
     *
     * @return array
     */
    abstract public function rules(): array;
}